"""
Adélie Linux System Site
Display information about owned/hosted hw
core.py - Core routines
Copyright © 2017 Adélie Linux Team.  All rights reserved.
NCSA license
"""

from functools import wraps
import os

from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from yaml import load


class ImproperlyConfiguredException(Exception):
    """Raised when the application has not been properly configured."""
    pass


CONFIG_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           'config.yaml')
"""The path on-disk to the application configuration file."""

if not os.path.exists(CONFIG_FILE):
    raise ImproperlyConfiguredException("No config file found.")

_CONFIG_STREAM = open(CONFIG_FILE, 'r')

CONFIG = load(_CONFIG_STREAM)
"""The loaded configuration object."""

_CONFIG_STREAM.close()

APP = Flask(__name__)
"""The Flask application handle."""

APP.config.update(CONFIG)
APP.config["SQLALCHEMY_DATABASE_URI"] = CONFIG['dburi']
APP.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

DATA = SQLAlchemy(APP)
"""The databae connection handle."""

def is_api():
    """Determine if the current request is an API request."""
    return request.is_xhr or ('Accept' in request.headers and
                              'json' in request.headers['Accept'])

def _(orig_str):
    """Translate a string.  For now, nothing."""
    return orig_str

def require_auth(view):
    """Deal with authentication.  For now, rely on apache2 htpasswd."""
    @wraps(view)
    def authed_view(*args, **kwargs):
        """Check for authentication."""
        # Put authentication checking logic here.
        return view(*args, **kwargs)
    return authed_view
