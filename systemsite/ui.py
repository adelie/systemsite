"""
Adélie Linux System Site
Display information about owned/hosted hw
ui.py - frontend
Copyright © 2017 Adélie Linux Team.  All rights reserved
NCSA license
"""

from flask import jsonify, render_template

from .core import APP, is_api
from .models import System

@APP.route("/")
def index():
    """Display the index page for the site.

    A compact list of the current public systems with links to see all of them
    with more information, or detail pages.
    """
    systems = System.query.order_by(System.nodename).all()

    if is_api():
        return jsonify({'systems': [sys.quick_serialise() for sys in systems]})
    else:
        return render_template('index.html', systems=systems)

@APP.route("/info/<nodename>")
def detail(nodename):
    """Display all details for the specified computer."""
    system = System.query.filter_by(nodename=nodename).first_or_404()

    if is_api():
        return jsonify(system.serialise())
    else:
        return render_template('detail.html', system=system)
