"""
Adélie Linux System Site
Display information about owned/hosted hw
models.py - database models
Copyright © 2017 Adélie Linux Team.  All rights reserved.
NCSA license
"""

from .core import DATA, _


class Serialisable():
    """Implements a JSON serialisation interface for database models.

    Given a `__serialise_props__` class property, return values for these
    properties from `serialise()`.  This must be specified; otherwise,
    ValueError will be raised when `serialise()` is called.

    Given a `__quick_serialise_props__` class property, return values for these
    properties from `quick_serialise()`.  If no `quick_serialise_props` is
    specified, use `serialise_props` instead.
    """
    def serialise(self):
        """Retrieve a JSON serialisation of the object."""

        props = getattr(self, '__serialise_props__', None)
        if props is None:
            raise ValueError(_("Model does not support serialisation."))

        return {prop: getattr(self, prop, None)
                for prop in props}

    def quick_serialise(self):
        """Retrieve a short JSON serialisation of the object."""

        props = getattr(self, '__quick_serialise_props__', None)
        if props is None:
            return self.serialise()

        return {prop: getattr(self, prop, None)
                for prop in props}


class Saveable():
    """Implements the ability to save a model."""
    def save(self):
        """Save the object."""
        DATA.session.add(self)
        DATA.session.commit()

    def delete(self):
        """Delete the object."""
        DATA.session.delete(self)
        DATA.session.commit()


class Contact(DATA.Model, Serialisable, Saveable):
    """Represents a contact for a system."""

    __serialise_props__ = ('id', 'name', 'handle')

    id = DATA.Column(DATA.Integer, primary_key=True)  # pylint: disable=C0103
    """The contact's ID."""
    name = DATA.Column(DATA.Text, unique=True, nullable=False)
    """The contact's preferred name."""
    handle = DATA.Column(DATA.String(64), unique=True, nullable=True)
    """The contact's handle.  This is for people who are part of the project."""
    email = DATA.Column(DATA.String(320), unique=True, nullable=False)
    """The contact's email address.  Won't be displayed publicly."""
    phone = DATA.Column(DATA.String(48), unique=True, nullable=True)
    """The contact's phone number.  Won't be displayed publicly."""
    systems = DATA.relationship('System', backref='contact')
    """The systems for which this contact is responsible."""

    def __repr__(self):
        """String representation of the contact."""
        return "Contact(%s)" % ", ".join("{k}={v}".format(k=k, v=repr(v))
                                         for k, v in self.serialise().items())


class System(DATA.Model, Serialisable, Saveable):
    """Represents a computer system owned by or hosted for Adélie."""

    __serialise_props__ = ('id', 'sys_type', 'nodename', 'cpu', 'mem_info',
                           'disk_info', 'roles', 'contact_id')
    __quick_serialise_props__ = ('id', 'nodename', 'cpu', 'roles', 'contact_id')

    id = DATA.Column(DATA.Integer, primary_key=True)  # pylint: disable=C0103
    """The system's unique ID."""
    sys_type = DATA.Column(DATA.Enum('server', 'workstation', 'desktop',
                                     'portable', 'soc', name='sys_types'),
                           nullable=False)
    """The type of this system."""
    nodename = DATA.Column(DATA.String(64), unique=True, nullable=False)
    """The system's nodename, or host name."""
    cpu = DATA.Column(DATA.Enum('x86', 'x86_64', 'ppc', 'ppc64', 'mips',
                                'mipsel', 'mips32', 'mips32el', 'sparc64',
                                'alpha', 'riscv',
                                name='cpus'), nullable=False)
    """The system's CPU type."""
    mem_info = DATA.Column(DATA.Text, nullable=False)
    """The system's memory information."""
    disk_info = DATA.Column(DATA.Text, nullable=False)
    """The system's disk information."""
    roles = DATA.Column(DATA.Text, nullable=False)
    """The system's role(s) in Adélie."""
    contact_id = DATA.Column(DATA.Integer, DATA.ForeignKey('contact.id'),
                             nullable=False)
    """The system's contact."""
    notes = DATA.Column(DATA.Text, nullable=True)
    """Any other notes about the system."""

    def __repr__(self):
        """String representation of the system."""
        return "System(%s)" % ", ".join("{k}={v}".format(k=k, v=repr(v))
                                        for k, v in self.serialise().items())
