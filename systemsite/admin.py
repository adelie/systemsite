"""
Adélie Linux System Site
Display information about owned/hosted hw
admin.py - manipulate objects
Copyright © 2017 Adélie Linux Team.  All rights reserved
NCSA license
"""

from flask import request, jsonify, render_template, redirect, url_for

from .core import APP, is_api, require_auth
from .models import Contact, System

@APP.route('/admin')
@require_auth
def admin_tasks():
    """Display a list of administrator tasks."""
    if is_api():
        # This confirms to the client that authentication was good.
        return jsonify('ok')
    else:
        return render_template('admin/tasks.html')


@APP.route('/admin/system/new')
@require_auth
def new_system():
    """Display the new system form."""
    return render_template('admin/system_form.html',
                           contacts=Contact.query.all())


@APP.route('/admin/system', methods=('POST',))
@require_auth
def create_system():
    """Create the new system."""
    system = System()
    system.sys_type = request.form['sys_type'].lower()
    for attr in ('nodename', 'cpu', 'mem_info', 'disk_info', 'roles',
                 'contact_id', 'notes'):
        setattr(system, attr, request.form.get(attr, None))

    system.save()
    return redirect(url_for('admin_tasks'))


@APP.route('/admin/contact/new')
@require_auth
def new_contact():
    """Display the new contact form."""
    return render_template('admin/contact_form.html')


@APP.route('/admin/contact', methods=('POST',))
@require_auth
def create_contact():
    """Create the new contact."""
    contact = Contact()
    for attr in ('name', 'handle', 'email', 'phone'):
        setattr(contact, attr, request.form.get(attr, None))

    contact.save()
    return redirect(url_for('admin_tasks'))
