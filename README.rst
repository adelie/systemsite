=====================================
 README for Adélie Linux System Site
=====================================
:Authors:
 * **A. Wilcox**, primary developer
:Status:
 Production
:Copyright:
 © 2017 Adélie Linux.  NCSA open source license.



Introduction
============

This distribution contains the Web site for displaying information about
hardware systems ran by or hosted for Adélie Linux.  It allows both users
and other developers to discover what systems the Adélie Linux distribution
uses, what their purposes are, how to contact their owners, and more.


License
```````
This Web site is licensed under a NCSA license.


Changes
```````
Any changes to this repository must be reviewed before being pushed to the
master branch.




Requirements
============

Background
``````````

Adélie, like many other Linux distributions, requires a lot of complex server
infrastructure for many different tasks: email, package building, mirrors, and
Web sites, to name a few.  As such, there are many servers within Adélie.  It
is not always clear who manages a server, or all of its purposes.

Many times, people ping me (awilfox@) when I am not even the one in control of
the server, because I am the distro lead.  Obviously that does not help if I
cannot provide assistance, and it wastes not only my time, but the time of the
requestor.  A system that could allow people to discover who to contact with a
problem would save everyone time.

Additionally, having a central repository of all of our computer systems allows
us to know where we may have spare capacity, or may need to rearchitect to
avoid wasting resources.


Major features
``````````````

#. Provide a list of all computer systems owned by or hosted for Adélie Linux,
   including the following information:

  #. Hardware type (server, workstation).

  #. CPU type (PowerPC, MIPS, others).

  #. Memory size and type, where known.

  #. Disk size and type.

  #. Operating environment (Adélie, Gentoo, FreeBSD, others).

  #. Contact information for the local system administrator.

#. Allow easy manipulation of the data to ensure data is kept current.

#. Provide signed in users with additional information including login or IP
   information.

  #. Remote access options available (SSH, HTTPS, others).

  #. IP address(es), v4 and v6.

