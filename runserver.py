from systemsite.core import APP, CONFIG
import systemsite.ui
if CONFIG.get('admin', False):
    import systemsite.admin
from sys import argv

APP.run(debug=CONFIG.get('debug', True), host=CONFIG.get('host', '::'),
        port=int(CONFIG.get('port', 8000)))
